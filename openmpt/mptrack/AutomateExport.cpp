#include "stdafx.h"
#include "Mptrack.h"
#include "Mainfrm.h"
#include "InputHandler.h"
#include "Moddoc.h"
#include "ModDocTemplate.h"
#include "../soundlib/mod_specifications.h"
#include "../soundlib/plugins/PlugInterface.h"
#include "Childfrm.h"
#include "Mpdlgs.h"
#include "dlg_misc.h"
#include "TempoSwingDialog.h"
#include "mod2wave.h"
#include "ChannelManagerDlg.h"
#include "MIDIMacroDialog.h"
#include "MIDIMappingDialog.h"
#include "StreamEncoderAU.h"
#include "StreamEncoderFLAC.h"
#include "StreamEncoderMP3.h"
#include "StreamEncoderOpus.h"
#include "StreamEncoderRAW.h"
#include "StreamEncoderVorbis.h"
#include "StreamEncoderWAV.h"
#include "mod2midi.h"
#include "../common/version.h"
#include "../tracklib/SampleEdit.h"
#include "../soundlib/modsmp_ctrl.h"
#include "CleanupSong.h"
#include "../common/mptStringBuffer.h"
#include "../common/mptFileIO.h"
#include <sstream>
#include "../common/FileReader.h"
#include "FileDialog.h"
#include "ExternalSamples.h"
#include "Globals.h"
#include "../soundlib/OPL.h"
#ifndef NO_PLUGINS
#include "AbstractVstEditor.h"
#endif
#include "../soundlib/plugins/PluginManager.h"
#include "AutomateExport.h"
#include "../mptrack/plugins/VstDefinitions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


OPENMPT_NAMESPACE_BEGIN



void CModDoc::OnFileSaveaswav()
{
	// TODO: ここにコマンド ハンドラー コードを追加します。
	std::vector<EncoderFactoryBase *> encoders;
	WAVEncoder enc;
	if(enc.IsAvailable())
	{
		encoders.push_back(&enc);
	}

	CWaveConvertSettings Settings(theApp.GetSettings(), encoders);
	Settings.FinalSampleFormat = SampleFormatFloat32;
	{
		//encSettings.Format
		DWORD_PTR dwFormat = Settings.GetEncoderSettings().Format;     //GetEncoderSettings().Format();
		int format = (int)((dwFormat >> 0) & 0xffff);
		if(enc.GetTraits().formats[format].Sampleformat == SampleFormatInvalid)
		{
			Settings.FinalSampleFormat = SampleFormatFloat32;
		} else
		{
			Settings.FinalSampleFormat = enc.GetTraits().formats[format].Sampleformat;
		}
	}

	mpt::PathString ext = mpt::PathString::FromCString(CString(".wav"));
	OnFileWaveConvert(ext, &Settings );
}


void CModDoc::OnFileSaveasmp3()
{
	// TODO: ここにコマンド ハンドラー コードを追加します。
	std::vector<EncoderFactoryBase *> encoders;
#if 0
	MP3Encoder mp3lame(MP3EncoderLame);
	if(mp3lame.IsAvailable())
	{
		encoders.push_back(&mp3lame);
	}
#endif
	MP3Encoder mp3lamecompatible(MP3EncoderLameCompatible);
	if(mp3lamecompatible.IsAvailable())
	{
		encoders.push_back(&mp3lamecompatible);
	}

	CWaveConvertSettings Settings(theApp.GetSettings(), encoders);
	Settings.FinalSampleFormat = SampleFormatFloat32;
	mpt::PathString ext = mpt::PathString::FromCString(CString(".mp3"));
	OnFileWaveConvert(ext, &Settings );
}

#if 1
void CModDoc::OnFileWaveConvert(const mpt::PathString fileExt, CWaveConvertSettings *Settings )
{

	mpt::PathString fileName;
	{
		mpt::PathString orgPathName = mpt::PathString::FromCString(GetPathName());
		mpt::PathString drive ;
		mpt::PathString dir ;
		mpt::PathString basename ;
		orgPathName.SplitPath(&drive, &dir, &basename, NULL);
		fileName = drive + dir + basename;
	}

	size_t index = 0 ;//Settings->FindEncoder(encoderSettingsName);
	Settings->SelectEncoder(index);
	EncoderFactoryBase *encFactorieForUse = Settings->GetEncoderFactory();

	ASSERT(encFactorieForUse != NULL);
	CMainFrame *pMainFrm = CMainFrame::GetMainFrame();

	if((!pMainFrm) || (!m_SndFile.GetType()) || encFactorieForUse == NULL)
		return;

	const ORDERINDEX currentOrd = m_SndFile.m_PlayState.m_nCurrentOrder;
	const ROWINDEX currentRow = m_SndFile.m_PlayState.m_nRow;

	int nRenderPasses = 1;
	// Channel mode
	std::vector<bool> usedChannels;
	std::vector<FlagSet<ChannelFlags>> channelFlags;
	// Instrument mode
	std::vector<bool> instrMuteState;
#if 0
	// Channel mode: save song in multiple wav files (one for each enabled channels)
	if(bChannelMode)  //wsdlg.m_bChannelMode=false
	{
		// Don't save empty channels
		CheckUsedChannels(usedChannels);

		nRenderPasses = m_SndFile.GetNumChannels();
		channelFlags.resize(nRenderPasses, ChannelFlags(0));
		for(CHANNELINDEX i = 0; i < m_SndFile.GetNumChannels(); i++)
		{
			// Save channels' flags
			channelFlags[i] = m_SndFile.ChnSettings[i].dwFlags;
			// Ignore muted channels
			if(channelFlags[i][CHN_MUTE])
				usedChannels[i] = false;
			// Mute each channel
			m_SndFile.ChnSettings[i].dwFlags.set(CHN_MUTE);
		}
	}
	// Instrument mode: Same as channel mode, but renders per instrument (or sample)
	if(bInstrumentMode)  //wsdlg.m_bInstrumentMode=false
	{
		if(m_SndFile.GetNumInstruments() == 0)
		{
			nRenderPasses = m_SndFile.GetNumSamples();
			instrMuteState.resize(nRenderPasses, false);
			for(SAMPLEINDEX i = 0; i < m_SndFile.GetNumSamples(); i++)
			{
				instrMuteState[i] = IsSampleMuted(i + 1);
				MuteSample(i + 1, true);
			}
		} else
		{
			nRenderPasses = m_SndFile.GetNumInstruments();
			instrMuteState.resize(nRenderPasses, false);
			for(INSTRUMENTINDEX i = 0; i < m_SndFile.GetNumInstruments(); i++)
			{
				instrMuteState[i] = IsInstrumentMuted(i + 1);
				MuteInstrument(i + 1, true);
			}
		}
	}
#endif
	pMainFrm->PauseMod(this);
	int oldRepeat = m_SndFile.GetRepeatCount();

	const SEQUENCEINDEX currentSeq = m_SndFile.Order.GetCurrentSequenceIndex();
	//for(SEQUENCEINDEX seq = Settings->minSequence; seq <= Settings->maxSequence; seq++)  //wsdlg.m_Settings.minSequence= 0 maxSequence=0
	{
		m_SndFile.Order.SetSequence(0);
		std::string fileNameAdd;
		for(int i = 0; i < nRenderPasses; i++)
		{
			mpt::PathString thisName = fileName;
			CString caption = _T("file");
			fileNameAdd.clear();
#if 0
			if(Settings->minSequence != Settings->maxSequence)
			{
				fileNameAdd = mpt::format("-%1")(mpt::fmt::dec0<2>(seq + 1));
				std::string seqName = m_SndFile.Order(seq).GetName();
				if(!seqName.empty())
				{
					fileNameAdd += "-" + seqName;
				}
			}
#endif
#if 0
			// Channel mode
			if(bChannelMode)  //wsdlg.m_bChannelMode=false
			{
				// Re-mute previously processed channel
				if(i > 0)
					m_SndFile.ChnSettings[i - 1].dwFlags.set(CHN_MUTE);

				// Was this channel actually muted? Don't process it then.
				if(!usedChannels[i])
					continue;

				// Add channel number & name (if available) to path string
				if(!m_SndFile.ChnSettings[i].szName.empty())
				{
					fileNameAdd += mpt::format("-%1_%2")(mpt::fmt::dec0<3>(i + 1), m_SndFile.ChnSettings[i].szName);
					caption.Format(_T("%u:"), i + 1);
					caption += mpt::ToCString(m_SndFile.GetCharsetInternal(), m_SndFile.ChnSettings[i].szName);
				} else
				{
					fileNameAdd += mpt::format("-%1")(mpt::fmt::dec0<3>(i + 1));
					caption.Format(_T("channel %u"), i + 1);
				}
				// Unmute channel to process
				m_SndFile.ChnSettings[i].dwFlags.reset(CHN_MUTE);
			}
			// Instrument mode
			if(bInstrumentMode)  //wsdlg.m_bInstrumentMode=false
			{
				if(m_SndFile.GetNumInstruments() == 0)
				{
					// Re-mute previously processed sample
					if(i > 0)
						MuteSample(static_cast<SAMPLEINDEX>(i), true);

					if(!m_SndFile.GetSample(static_cast<SAMPLEINDEX>(i + 1)).HasSampleData() || !IsSampleUsed(static_cast<SAMPLEINDEX>(i + 1), false) || instrMuteState[i])
						continue;

					// Add sample number & name (if available) to path string
					if(!m_SndFile.m_szNames[i + 1].empty())
					{
						fileNameAdd += mpt::format("-%1_%2")(mpt::fmt::dec0<3>(i + 1), m_SndFile.m_szNames[i + 1]);
						caption.Format(_T("%u: "), i + 1);
						caption += mpt::ToCString(m_SndFile.GetCharsetInternal(), m_SndFile.m_szNames[i + 1]);
					} else
					{
						fileNameAdd += mpt::format("-%1")(mpt::fmt::dec0<3>(i + 1));
						caption.Format(_T("sample %u"), i + 1);
					}
					// Unmute sample to process
					MuteSample(static_cast<SAMPLEINDEX>(i + 1), false);
				} else
				{
					// Re-mute previously processed instrument
					if(i > 0)
						MuteInstrument(static_cast<INSTRUMENTINDEX>(i), true);

					if(m_SndFile.Instruments[i + 1] == nullptr || !IsInstrumentUsed(static_cast<SAMPLEINDEX>(i + 1), false) || instrMuteState[i])
						continue;

					if(!m_SndFile.Instruments[i + 1]->name.empty())
					{
						fileNameAdd += mpt::format("-%1_%2")(mpt::fmt::dec0<3>(i + 1), m_SndFile.Instruments[i + 1]->name);
						caption.Format(_T("%u:"), i + 1);
						caption += mpt::ToCString(m_SndFile.GetCharsetInternal(), m_SndFile.Instruments[i + 1]->name);
					} else
					{
						fileNameAdd += mpt::format("-%1")(mpt::fmt::dec0<3>(i + 1));
						caption.Format(_T("instrument %u"), i + 1);
					}
					// Unmute instrument to process
					MuteInstrument(static_cast<SAMPLEINDEX>(i + 1), false);
				}
			}
#endif
			if(!fileNameAdd.empty())
			{
				SanitizeFilename(fileNameAdd);
				thisName += mpt::PathString::FromUnicode(mpt::ToUnicode(mpt::CharsetLocale, fileNameAdd));
			}
			thisName += fileExt;
#if 0
			if(Settings->outputToSample)  //wsdlg.m_Settings.outputToSample=false
			{
				thisName = mpt::CreateTempFileName(P_("OpenMPT"));
				// Ensure this temporary file is marked as temporary in the file system, to increase the chance it will never be written to disk
				::CloseHandle(::CreateFile(thisName.AsNative().c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL));
			}
#endif
			// Render song (or current channel, or current sample/instrument)
			bool cancel = true;
			try
			{
				mpt::SafeOutputFile safeFileStream(thisName, std::ios::binary, mpt::FlushModeFromBool(TrackerSettings::Instance().MiscFlushFileBuffersOnSave));
				mpt::ofstream &f = safeFileStream;
				// Do not enable yet, WAVWriter destructor may throw
				//f.exceptions(f.exceptions() | std::ios::badbit | std::ios::failbit);

				if(!f)
				{
					Reporting::Error("Could not open file for writing. Is it open in another application?");
				} else
				{
#if 1
					{
						BeginWaitCursor();
						CDoWaveConvert_Run_core(*Settings, m_SndFile, f);
						EndWaitCursor();
					}
#else
					BypassInputHandler bih;
					CDoWaveConvert dwcdlg(m_SndFile, f, caption, *Settings, pMainFrm);
					dwcdlg.m_bGivePlugsIdleTime = false;  //wsdlg.m_bGivePlugsIdleTime;
					dwcdlg.m_dwSongLimit = 0;
						//wsdlg.m_dwSongLimit;
					cancel = dwcdlg.DoModal() != IDOK;
#endif
				}
			} catch(const std::exception &)
			{
				Reporting::Error(_T("Error while writing file!"));
			}
#if 0
			if(Settings->outputToSample)  //wsdlg.m_Settings.outputToSample=false
			{
				if(!cancel)
				{
					InputFile f(thisName, TrackerSettings::Instance().MiscCacheCompleteFileBeforeLoading);
					if(f.IsValid())
					{
						FileReader file = GetFileReader(f);
						SAMPLEINDEX smp = Settings->sampleSlot;
						if(smp == 0 || smp > GetNumSamples())
							smp = m_SndFile.GetNextFreeSample();
						if(smp == SAMPLEINDEX_INVALID)
						{
							Reporting::Error(_T("Too many samples!"));
							cancel = true;
						}
						if(!cancel)
						{
							if(GetNumSamples() < smp)
								m_SndFile.m_nSamples = smp;
							GetSampleUndo().PrepareUndo(smp, sundo_replace, "Render To Sample");
							if(m_SndFile.ReadSampleFromFile(smp, file, false))
							{
								m_SndFile.m_szNames[smp] = "Render To Sample" + fileNameAdd;
								UpdateAllViews(nullptr, SampleHint().Info().Data().Names());
								if(m_SndFile.GetNumInstruments() && !IsSampleUsed(smp))
								{
									// Insert new instrument for the generated sample in case it is not referenced by any instruments yet.
									// It should only be already referenced if the user chose to export to an existing sample slot.
									InsertInstrument(smp);
									UpdateAllViews(nullptr, InstrumentHint().Info().Names());
								}
								SetModified();
							} else
							{
								GetSampleUndo().RemoveLastUndoStep(smp);
							}
						}
					}
				}

				// Always clean up after ourselves
				for(int retry = 0; retry < 10; retry++)
				{
					// stupid virus scanners
					if(DeleteFile(thisName.AsNative().c_str()) != EACCES)
					{
						break;
					}
					Sleep(10);
				}
			}

			if(cancel)
				break;
#endif
		}
	}
#if 0
	// Restore channels' flags
	if(bChannelMode)
	{
		for(CHANNELINDEX i = 0; i < m_SndFile.GetNumChannels(); i++)
		{
			m_SndFile.ChnSettings[i].dwFlags = channelFlags[i];
		}
	}
	// Restore instruments' / samples' flags
	if(bInstrumentMode)
	{
		for(size_t i = 0; i < instrMuteState.size(); i++)
		{
			if(m_SndFile.GetNumInstruments() == 0)
				MuteSample(static_cast<SAMPLEINDEX>(i + 1), instrMuteState[i]);
			else
				MuteInstrument(static_cast<INSTRUMENTINDEX>(i + 1), instrMuteState[i]);
		}
	}
#endif
	m_SndFile.Order.SetSequence(currentSeq);
	m_SndFile.SetRepeatCount(oldRepeat);
	m_SndFile.GetLength(eAdjust, GetLengthTarget(currentOrd, currentRow));
	m_SndFile.m_PlayState.m_nNextOrder = currentOrd;
	m_SndFile.m_PlayState.m_nNextRow = currentRow;
	CMainFrame::UpdateAudioParameters(m_SndFile, true);
}
#endif

extern CSoundFile::samplecount_t ReadInterleaved(CSoundFile &sndFile, void *outputBuffer, CSoundFile::samplecount_t count, SampleFormat sampleFormat, Dither &dither);



void CDoWaveConvert_Run_core(CWaveConvertSettings &settings, CSoundFile &sndFile, mpt::ofstream &fileStream)
{
#if 1
	static char buffer[MIXBUFFERSIZE * 4 * 4];             // channels * sizeof(biggestsample)
	static MixSampleFloat floatbuffer[MIXBUFFERSIZE * 4];  // channels
	static MixSampleInt mixbuffer[MIXBUFFERSIZE * 4];      // channels

	UINT ok = IDOK;
	uint64 ullSamples = 0;

	#if 0
	float normalizePeak = 0.0f;

	const mpt::PathString normalizeFileName = mpt::CreateTempFileName(P_("OpenMPT"));
	mpt::fstream normalizeFile;
	if(settings.normalize)
	{
		// Ensure this temporary file is marked as temporary in the file system, to increase the chance it will never be written to disk
		::CloseHandle(::CreateFile(normalizeFileName.AsNative().c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL));

		normalizeFile.open(normalizeFileName, std::ios::binary | std::ios::in | std::ios::out | std::ios::trunc);
	}
#endif
	Encoder::Settings &encSettings = settings.GetEncoderSettings();
	const uint32 samplerate = encSettings.Samplerate;
	const uint16 channels = encSettings.Channels;

	ASSERT(settings.GetEncoderFactory() && settings.GetEncoderFactory()->IsAvailable());

	// Silence mix buffer of plugins, for plugins that don't clear their reverb buffers and similar stuff when they are reset
#ifndef NO_PLUGINS
	if(settings.silencePlugBuffers)
	{
		//SetText(_T("Clearing plugin buffers"));
		for(auto &plug : sndFile.m_MixPlugins)
		{
			if(plug.pMixPlugin != nullptr)
			{
				// Render up to 20 seconds per plugin
				for(int j = 0; j < 20; j++)
				{
					const float maxVal = plug.pMixPlugin->RenderSilence(samplerate);
					if(maxVal <= FLT_EPSILON)
					{
						break;
					}
				}

				//ProcessMessages();
				//if(m_abort)
				//{
					//m_abort = false;
				//	break;
				//}
			}
		}
	}
#endif  // NO_PLUGINS

	MixerSettings oldmixersettings = sndFile.m_MixerSettings;
	MixerSettings mixersettings = TrackerSettings::Instance().GetMixerSettings();
	mixersettings.m_nMaxMixChannels = MAX_CHANNELS;  // always use max mixing channels when rendering
	mixersettings.gdwMixingFreq = samplerate;
	mixersettings.gnChannels = channels;
	sndFile.m_SongFlags.reset(SONG_PAUSED | SONG_STEP);
	if(settings.normalize)
	{
#ifndef NO_AGC
		mixersettings.DSPMask &= ~SNDDSP_AGC;
#endif
	}

	Dither dither(theApp.PRNG());
	dither.SetMode((DitherMode)encSettings.Dither.Get());

	sndFile.ResetChannels();
	sndFile.SetMixerSettings(mixersettings);
	sndFile.SetResamplerSettings(TrackerSettings::Instance().GetResamplerSettings());
	sndFile.InitPlayer(true);

	// Tags must be known at the stream start,
	// so that the encoder class could write them before audio data if mandated by the format,
	// otherwise they should just be cached by the encoder.
	std::unique_ptr<IAudioStreamEncoder> fileEnc = settings.GetEncoderFactory()->ConstructStreamEncoder(fileStream, encSettings, settings.Tags);

	uint64 ullMaxSamples = uint64_max / (channels * ((settings.FinalSampleFormat.GetBitsPerSample() + 7) / 8));
	//if(m_dwSongLimit)
	//{
	//	LimitMax(ullMaxSamples, m_dwSongLimit * samplerate);
	//}

	// Calculate maximum samples
	uint64 max = /* m_dwSongLimit ? ullMaxSamples : */uint64_max;

	// Reset song position tracking
	sndFile.ResetPlayPos();
	sndFile.m_SongFlags.reset(SONG_PATTERNLOOP);
	ORDERINDEX startOrder = 0;
	GetLengthTarget target;
	if(settings.minOrder != ORDERINDEX_INVALID && settings.maxOrder != ORDERINDEX_INVALID)
	{
		sndFile.SetRepeatCount(0);
		startOrder = settings.minOrder;
		ORDERINDEX endOrder = settings.maxOrder;
		while(!sndFile.Order().IsValidPat(endOrder) && endOrder > startOrder)
		{
			endOrder--;
		}
		if(sndFile.Order().IsValidPat(endOrder))
		{
			target = GetLengthTarget(endOrder, sndFile.Patterns[sndFile.Order()[endOrder]].GetNumRows() - 1);
		}
		target.StartPos(sndFile.Order.GetCurrentSequenceIndex(), startOrder, 0);
		sndFile.m_nMaxOrderPosition = endOrder + 1;
	} else
	{
		sndFile.SetRepeatCount(std::max(0, settings.repeatCount - 1));
	}
	uint64 l = mpt::saturate_round<uint64>(sndFile.GetLength(eNoAdjust, target).front().duration * samplerate * (1 + sndFile.GetRepeatCount()));

	sndFile.SetCurrentOrder(startOrder);
	sndFile.GetLength(eAdjust, GetLengthTarget(startOrder, 0));  // adjust playback variables / visited rows vector
	sndFile.m_PlayState.m_nCurrentOrder = startOrder;

	if(l < max)
		max = l;

	//SetRange(0, max);
	//EnableTaskbarProgress();

	// No pattern cue points yet
	std::vector<PatternCuePoint> patternCuePoints;
	patternCuePoints.reserve(sndFile.Order().size());
	sndFile.m_PatternCuePoints = &patternCuePoints;

	//CString progressStr;
	//if(settings.normalize)
	//	progressStr = _T("Rendering ") + caption + _T("... (%umn%02us, %umn%02us remaining)");
	//else
	//	progressStr = _T("Writing ") + caption + _T("... (%lluKB, %umn%02us, %umn%02us remaining)");

	// Process the conversion

	// For calculating the remaining time
	auto dwStartTime = timeGetTime(), prevTime = dwStartTime;
	uint32 timeRemaining = 0;

	uint64 bytesWritten = 0;

	auto mainFrame = CMainFrame::GetMainFrame();
	mainFrame->PauseMod();
	sndFile.m_SongFlags.reset(SONG_STEP | SONG_PATTERNLOOP);
	mainFrame->InitRenderer(&sndFile);

	for(UINT n = 0;; n++)
	{
		UINT lRead = 0;
		if(settings.normalize || settings.FinalSampleFormat == SampleFormatFloat32)
		{
			lRead = ReadInterleaved(sndFile, floatbuffer, MIXBUFFERSIZE, SampleFormatFloat32, dither);
		} else
		{
			lRead = ReadInterleaved(sndFile, buffer, MIXBUFFERSIZE, settings.FinalSampleFormat, dither);
		}

		// Process cue points (add base offset), if there are any to process.
		for(auto iter = patternCuePoints.rbegin(); iter != patternCuePoints.rend(); ++iter)
		{
			if(iter->processed)
			{
				// From this point, all cues have already been processed.
				break;
			}
			iter->offset += ullSamples;
			iter->processed = true;
		}

//		if(m_bGivePlugsIdleTime)
//		{
//			Sleep(20);
//		}

		if(!lRead)
			break;
		ullSamples += lRead;
#if 0
		if(settings.normalize)
		{

			std::size_t countSamples = lRead * sndFile.m_MixerSettings.gnChannels;
			const float *src = floatbuffer;
			while(countSamples--)
			{
				const float val = *src;
				if(val > normalizePeak)
					normalizePeak = val;
				else if(0.0f - val >= normalizePeak)
					normalizePeak = 0.0f - val;
				src++;
			}

			normalizeFile.write(reinterpret_cast<const char *>(floatbuffer), lRead * sndFile.m_MixerSettings.gnChannels * sizeof(float));
			if(!normalizeFile)
				break;

		} else
#endif
		{

			const std::streampos oldPos = fileStream.tellp();
			if(settings.FinalSampleFormat == SampleFormatFloat32)
			{
				fileEnc->WriteInterleaved(lRead, floatbuffer);
			} else
			{
				MPT_ASSERT(!mpt::endian_is_weird());
				if(fileEnc->GetConvertedEndianness() != mpt::get_endian())
				{
					mpt::SwapBufferEndian(settings.FinalSampleFormat.GetBitsPerSample() / 8, buffer, lRead * encSettings.Channels);
				}
				fileEnc->WriteInterleavedConverted(lRead, buffer);
			}
			const std::streampos newPos = fileStream.tellp();
			bytesWritten += static_cast<uint64>(newPos - oldPos);

			if(!fileStream)
			{
				break;
			}
		}
//		if(m_dwSongLimit && (ullSamples >= ullMaxSamples))
//		{
//			break;
//		}

		auto currentTime = timeGetTime();
		if((currentTime - prevTime) >= 16)
		{
			prevTime = currentTime;

			DWORD seconds = (DWORD)(ullSamples / sndFile.m_MixerSettings.gdwMixingFreq);

			if((ullSamples > 0) && (ullSamples < max))
			{
				timeRemaining = static_cast<uint32>((timeRemaining + ((currentTime - dwStartTime) * (max - ullSamples) / ullSamples) / 1000) / 2);
			}
//
//			if(settings.normalize)
//				_stprintf(s, progressStr, seconds / 60, seconds % 60, timeRemaining / 60, timeRemaining % 60u);
//			else
//				_stprintf(s, progressStr, bytesWritten >> 10, seconds / 60, seconds % 60u, timeRemaining / 60, timeRemaining % 60u);
//			SetText(s);
//
//			SetProgress(ullSamples);
		}
//		ProcessMessages();

//		if(m_abort)
//		{
//			ok = IDCANCEL;
//			break;
//		}
	}

	sndFile.m_nMaxOrderPosition = 0;

	mainFrame->StopRenderer(&sndFile);

#if 0
	if(settings.normalize)
	{
		const float normalizeFactor = (normalizePeak != 0.0f) ? (1.0f / normalizePeak) : 1.0f;

		const uint64 framesTotal = ullSamples;
		int lastPercent = -1;

		normalizeFile.seekp(0);

		uint64 framesProcessed = 0;
		uint64 framesToProcess = framesTotal;

//		SetRange(0, framesTotal);

		while(framesToProcess)
		{
			const std::size_t framesChunk = std::min<std::size_t>(mpt::saturate_cast<std::size_t>(framesToProcess), MIXBUFFERSIZE);
			const uint32 samplesChunk = static_cast<uint32>(framesChunk * channels);

			normalizeFile.read(reinterpret_cast<char *>(floatbuffer), samplesChunk * sizeof(float));
			if(normalizeFile.gcount() != static_cast<std::streamsize>(samplesChunk * sizeof(float)))
				break;

			for(std::size_t i = 0; i < samplesChunk; ++i)
			{
				floatbuffer[i] *= normalizeFactor;
			}

			const std::streampos oldPos = fileStream.tellp();
			if(settings.FinalSampleFormat == SampleFormatFloat32)
			{
				fileEnc->WriteInterleaved(framesChunk, floatbuffer);
			} else
			{
				// Convert float buffer to mixbuffer format so we can apply dither.
				// This can probably be changed in the future when dither supports floating point input directly.
				FloatToMonoMix(floatbuffer, mixbuffer, samplesChunk, MIXING_SCALEF);
				switch(settings.FinalSampleFormat.value)
				{
				case SampleFormatUnsigned8:
					dither.WithDither(
					    [&](auto &ditherInstance) {
						    return ConvertInterleavedFixedPointToInterleaved<MIXING_FRACTIONAL_BITS, false>(reinterpret_cast<uint8 *>(buffer), mixbuffer, ditherInstance, channels, framesChunk);
					    });
					break;
				case SampleFormatInt8:
					dither.WithDither(
					    [&](auto &ditherInstance) {
						    return ConvertInterleavedFixedPointToInterleaved<MIXING_FRACTIONAL_BITS, false>(reinterpret_cast<int8 *>(buffer), mixbuffer, ditherInstance, channels, framesChunk);
					    });
					break;
				case SampleFormatInt16:
					dither.WithDither(
					    [&](auto &ditherInstance) {
						    return ConvertInterleavedFixedPointToInterleaved<MIXING_FRACTIONAL_BITS, false>(reinterpret_cast<int16 *>(buffer), mixbuffer, ditherInstance, channels, framesChunk);
					    });
					break;
				case SampleFormatInt24:
					dither.WithDither(
					    [&](auto &ditherInstance) {
						    return ConvertInterleavedFixedPointToInterleaved<MIXING_FRACTIONAL_BITS, false>(reinterpret_cast<int24 *>(buffer), mixbuffer, ditherInstance, channels, framesChunk);
					    });
					break;
				case SampleFormatInt32:
					dither.WithDither(
					    [&](auto &ditherInstance) {
						    return ConvertInterleavedFixedPointToInterleaved<MIXING_FRACTIONAL_BITS, false>(reinterpret_cast<int32 *>(buffer), mixbuffer, ditherInstance, channels, framesChunk);
					    });
					break;
				default: MPT_ASSERT(false); break;
				}
				MPT_ASSERT(!mpt::endian_is_weird());
				if(fileEnc->GetConvertedEndianness() != mpt::get_endian())
				{
					mpt::SwapBufferEndian(settings.FinalSampleFormat.GetBitsPerSample() / 8, buffer, framesChunk * channels);
				}
				fileEnc->WriteInterleavedConverted(framesChunk, buffer);
			}
			const std::streampos newPos = fileStream.tellp();
			bytesWritten += static_cast<std::size_t>(newPos - oldPos);

			auto currentTime = timeGetTime();
			if((currentTime - prevTime) >= 16)
			{
				prevTime = currentTime;

				int percent = static_cast<int>(100 * framesProcessed / framesTotal);
				if(percent != lastPercent)
				{
					_stprintf(s, _T("Normalizing... (%d%%)"), percent);
//					SetText(s);
//					SetProgress(framesProcessed);
					lastPercent = percent;
				}
//				ProcessMessages();
			}

			framesProcessed += framesChunk;
			framesToProcess -= framesChunk;
		}

		normalizeFile.flush();
		normalizeFile.close();
		for(int retry = 0; retry < 10; retry++)
		{
			// stupid virus scanners
			if(DeleteFile(normalizeFileName.AsNative().c_str()) != EACCES)
			{
				break;
			}
			Sleep(10);
		}
	}
#endif
	if(!patternCuePoints.empty())
	{
		if(encSettings.Cues)
		{
			std::vector<uint64> cues;
			cues.reserve(patternCuePoints.size());
			for(const auto &cue : patternCuePoints)
			{
				cues.push_back(static_cast<uint32>(cue.offset));
			}
			fileEnc->WriteCues(cues);
		}
	}
	sndFile.m_PatternCuePoints = nullptr;

	fileEnc = nullptr;

	CMainFrame::UpdateAudioParameters(sndFile, TRUE);
	//EndDialog(ok);
#endif
}

VSTPluginLib* CTrackApp::FindPluginByName(const mpt::PathString & pluginName)
{
	// TODO: ここに実装コードを追加します.
	CVstPluginManager *pManager = GetPluginManager();
	if(pManager)
	{
		bool first = true;

		for(auto p : *pManager)
		{
			ASSERT(p);
			VSTPluginLib &plug = *p;
			mpt::ustring displayName = mpt::ToLowerCase(plug.libraryName.ToUnicode());
			if(displayName == mpt::ToLowerCase(pluginName.ToUnicode()))
			{
				return p;
			}
		}
	}
	return nullptr;
}

void CModDoc::SetVSTPlugin(VSTPluginLib* plugin,const mpt::PathString & ParamFile,bool removeOriginalSample)
{
	// TODO: ここに実装コードを追加します.
	CVstPluginManager *pManager = theApp.GetPluginManager();
	VSTPluginLib *pNewPlug = plugin;
	VSTPluginLib *pFactory = nullptr;
	IMixPlugin *pCurrentPlugin = nullptr;
	SNDMIXPLUGIN *pPlugin(nullptr);
	PLUGINDEX nPlugSlot (eAutomatePlugSlot);
	bool changed(false);

	if( 0 <= nPlugSlot && nPlugSlot < MAX_MIXPLUGINS)
	{
		pPlugin = &(GetSoundFile().m_MixPlugins[nPlugSlot]);
	}

	if (pPlugin) pCurrentPlugin = pPlugin->pMixPlugin;
	if ((pManager) && (pManager->IsValidPlugin(pNewPlug))) pFactory = pNewPlug;

	if (pFactory)
	{
		// Plugin selected
		if ((!pCurrentPlugin) || &pCurrentPlugin->GetPluginFactory() != pFactory)
		{
			CriticalSection cs;

			// Destroy old plugin, if there was one.
			pPlugin->Destroy();

			// Initialize plugin info
			MemsetZero(pPlugin->Info);
			pPlugin->Info.dwPluginId1 = pFactory->pluginId1;
			pPlugin->Info.dwPluginId2 = pFactory->pluginId2;
			pPlugin->editorX = pPlugin->editorY = int32_min;

#ifndef NO_VST
			if(pPlugin->Info.dwPluginId1 == Vst::kEffectMagic)
			{
				switch(pPlugin->Info.dwPluginId2)
				{
					// Enable drymix by default for these known plugins
				case Vst::FourCC("Scop"):
					pPlugin->SetWetMix();
					break;
				}
			}
#endif // NO_VST

			pPlugin->Info.szName = pFactory->libraryName.ToLocale();
			pPlugin->Info.szLibraryName = pFactory->libraryName.ToUTF8();

			cs.Leave();

			// Now, create the new plugin
			if(pManager && this)
			{
				pManager->CreateMixPlugin(*pPlugin, this->GetSoundFile());
				if (pPlugin->pMixPlugin)
				{
					IMixPlugin *p = pPlugin->pMixPlugin;
					const CString name = p->GetDefaultEffectName();
					if(!name.IsEmpty())
					{
						pPlugin->Info.szName = mpt::ToCharset(mpt::CharsetLocale, name);
					}
					// Check if plugin slot is already assigned to any instrument, and if not, create one.
					if(p->IsInstrument() && this->HasInstrumentForPlugin(nPlugSlot) == INSTRUMENTINDEX_INVALID)
					{
						this->InsertInstrumentForPlugin(nPlugSlot);
					}
					pCurrentPlugin = pPlugin->pMixPlugin;
				} else
				{
					MemsetZero(pPlugin->Info);
				}
			}
			{
				if(pCurrentPlugin != nullptr && pCurrentPlugin->LoadProgram(ParamFile))
				{
					int32 currentProg = pCurrentPlugin->GetCurrentProgram();
				}
			}

			changed = true;
		}
	} else if(pPlugin->IsValidPlugin())
	{
		// No effect
		CriticalSection cs;
		pPlugin->Destroy();
		// Clear plugin info
		MemsetZero(pPlugin->Info);
		changed = true;
	}
	UpdateAllViews(nullptr, GeneralHint().General());
	SetPluginToAllInstruments(eAutomatePlugSlot,removeOriginalSample);
}

void CModDoc::SetPluginToAllInstruments(PLUGINDEX plugindex,bool removeOriginalSample)
{
	PLUGINDEX nPlug = plugindex+1; // add index for "no plugin" .

	for ( int InstIndex = 0; InstIndex < MAX_INSTRUMENTS ; InstIndex++){
		ModInstrument *pIns = m_SndFile.Instruments[InstIndex];

		if (pIns)
		{

			if(nPlug >= 0 && nPlug <= MAX_MIXPLUGINS)
			{
				bool active = true;
				if (active && pIns->nMixPlug != nPlug)
				{
					pIns->nMixPlug = nPlug;
				}
	#ifndef NO_PLUGINS
				if(pIns->nMixPlug)
				{
					// we have selected a plugin that's not "no plugin"
					const SNDMIXPLUGIN &plugin = m_SndFile.m_MixPlugins[pIns->nMixPlug - 1];
					if(plugin.pMixPlugin != nullptr)
					{

						if(active && plugin.pMixPlugin->IsInstrument())
						{
							if(pIns->nMidiChannel == MidiNoChannel)
							{
								// If this plugin can recieve MIDI events and we have no MIDI channel
								// selected for this instrument, automatically select MIDI channel 1.
								pIns->nMidiChannel = MidiFirstChannel;
							}
							if(pIns->midiPWD == 0)
							{
								pIns->midiPWD = 2;
							}

							// If we just dialled up an instrument plugin, zap the sample assignments.
							const std::set<SAMPLEINDEX> referencedSamples = pIns->GetSamples();
							bool hasSamples = false;
							for(auto sample : referencedSamples)
							{
								if(sample > 0 && sample <= m_SndFile.GetNumSamples() && m_SndFile.GetSample(sample).HasSampleData())
								{
									hasSamples = true;
									break;
								}
							}
#if 1
							if(hasSamples && removeOriginalSample )
							{
								pIns->AssignSample(0);
							}
#endif
						}
						//return;
					}
				}
	#endif // NO_PLUGINS
			}
		}

	}
}


OPENMPT_NAMESPACE_END



