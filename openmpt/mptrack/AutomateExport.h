#pragma once
#include "ExceptionHandler.h"

class CMPTCommandLineInfo : public CCommandLineInfo
{
public:
	std::vector<mpt::PathString> m_fileNames;
	bool m_noDls = false, m_noPlugins = false, m_noAssembly = false, m_noSysCheck = false, m_noWine = false,
	     m_portable = false, m_noCrashHandler = false, m_debugCrashHandler = false, m_sharedInstance = false;

	bool m_noGUI = false;
	bool m_bOutputToMP3 = false;
	bool m_bOutputToWAV = false;
	bool m_bAutoExit = false;

	bool m_bPluginSpecified = false;
	bool m_bPluginParamSpecified = false;
	mpt::PathString m_PluginName;
	mpt::PathString m_PluginParam;
	bool m_bRemoveOriginalSample = false;

#ifdef ENABLE_TESTS
	bool m_noTests = false;
#endif

public:
	void ParseParam(LPCTSTR param, BOOL isFlag, BOOL isLast) override
	{
		if(isFlag)
		{
			if(!lstrcmpi(param, _T("PluginName")))
			{
				m_bPluginSpecified = true;
				return;
			}else{
				m_bPluginSpecified = false;
			}
			if(!lstrcmpi(param, _T("PluginParam")))
			{
				m_bPluginParamSpecified = true;
				return;
			}else{
				m_bPluginParamSpecified = false;
			}
			if(!lstrcmpi(param, _T("RemoveOriginalSample")))
			{
				m_bRemoveOriginalSample = true;
				return;
			}
			if(!lstrcmpi(param, _T("nologo")))
			{
				m_bShowSplash = FALSE;
				return;
			}
			if(!lstrcmpi(param, _T("nodls")))
			{
				m_noDls = true;
				return;
			}
			if(!lstrcmpi(param, _T("noplugs")))
			{
				m_noPlugins = true;
				return;
			}
			if(!lstrcmpi(param, _T("portable")))
			{
				m_portable = true;
				return;
			}
			if(!lstrcmpi(param, _T("fullMemDump")))
			{
				ExceptionHandler::fullMemDump = true;
				return;
			}
			if(!lstrcmpi(param, _T("noAssembly")))
			{
				m_noAssembly = true;
				return;
			}
			if(!lstrcmpi(param, _T("noSysCheck")))
			{
				m_noSysCheck = true;
				return;
			}
			if(!lstrcmpi(param, _T("noWine")))
			{
				m_noWine = true;
				return;
			}
			if(!lstrcmpi(param, _T("noCrashHandler")))
			{
				m_noCrashHandler = true;
				return;
			}
			if(!lstrcmpi(param, _T("DebugCrashHandler")))
			{
				m_debugCrashHandler = true;
				return;
			}
			if(!lstrcmpi(param, _T("shared")))
			{
				m_sharedInstance = true;
				return;
			}
			if(!lstrcmpi(param, _T("shared")))
			{
				m_sharedInstance = true;
				return;
			}

			if(!lstrcmpi(param, _T("noGUI")))
			{
				m_bShowSplash = FALSE;
				m_noGUI = true;
				m_bAutoExit = true;
				return;
			}
			if(!lstrcmpi(param, _T("OutputToMP3")))
			{
				m_bOutputToMP3 = true;
				return;
			}
			if(!lstrcmpi(param, _T("OutputToWAV")))
			{
				m_bOutputToWAV = true;
				return;
			}

#ifdef ENABLE_TESTS
			if(!lstrcmpi(param, _T("noTests")))
			{
				m_noTests = true;
				return;
			}
#endif
		} else
		{
			if(m_bPluginSpecified){
				m_PluginName= mpt::PathString::FromNative(param);
			}else if(m_bPluginParamSpecified){
				m_PluginParam= mpt::PathString::FromNative(param);
			}else{
				m_fileNames.push_back(mpt::PathString::FromNative(param));
				if(m_nShellCommand == FileNew)
					m_nShellCommand = FileOpen;
			}
			m_bPluginSpecified = false;
			m_bPluginParamSpecified = false;
		}
		CCommandLineInfo::ParseParam(param, isFlag, isLast);
	}
};
